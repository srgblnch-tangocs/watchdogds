# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2017 CELLS/Alba synchrotron"
__license__ = "GPLv3+"
__status__ = "development"

from setuptools import setup, find_packages
from version import version


setup(name="WatchdogDS",
      version=version(),
      description="Tango Device Server",
      author=__author__,
      author_email=__email__,
      long_description="Tango Device Server to have an agent in the DCS to "
                       "monitor a set of devices to collect information about "
                       "running and fault states, and also check and recover "
                       "if any of them hang",
      license=__license__,
      entry_points={'console_scripts': ['Watchdog = Watchdog:main', ],
                    'gui_scripts': []},
      packages=find_packages(),
      install_requires=["numpy>=1.12.1"]  # , "tango>=9.3.3"]
)
