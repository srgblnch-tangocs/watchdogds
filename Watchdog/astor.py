#!/usr/bin/env python
# -*- coding:utf-8 -*-

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016 CELLS/Alba synchrotron"
__license__ = "GPLv3+"
__status__ = "development"


from tango import Database, DeviceProxy


class Astor:
    """
    Extremelly simple implementation of the used api of fandango while
    it isn't available in python3.
    """
    __db = None

    __check_servers__list = None
    __check_servers__all = None
    __check_servers__not_found = None

    def __init__(self):
        self.__db = Database()

    # first descendant level

    def get_device_server(self, device_name: str):
        try:
            return self.__db.get_device_info(device_name).ds_full_name
        except:
            return None

    def stop_servers(self, servers_list: list):
        if not self.check_servers_names(servers_list):
            raise AssertionError("One or more unrecognized servers")
        done = True
        for server_name in servers_list:
            try:
                starter_device = self.__get_starter_device(server_name)
                if server_name in list(starter_device.RunningServers):
                    starter_device.DevStop(server_name)
                elif server_name not in list(starter_device.StoppedServers):
                    self.__get_admin_device(server_name).command_inout('Kill')
                else:
                    done = False
            except Exception:
                done = False
        return done

    def start_servers(self, servers_list: list):
        if not self.check_servers_names(servers_list):
            raise AssertionError("One or more unrecognized servers")
        done = True
        for server_name in servers_list:
            try:
                starter_device = self.__get_starter_device(server_name)
                if server_name in list(starter_device.StoppedServers):
                    starter_device.DevStart(server_name)
                else:
                    done = False
            except Exception:
                done = False
        return done

    # second descendant level

    def check_servers_names(self, servers_list: list) -> bool:
        self.__check_servers__all = None
        self.__check_servers__list = None
        self.__check_servers__not_found = None
        all_server_list = [server_name.lower()
                           for server_name in self.__db.get_server_list()]
        servers_list = [server_name.lower() for server_name in servers_list]
        found = 0
        not_found = []
        for existing_server_name in all_server_list:
            if existing_server_name in servers_list:
                found += 1
            else:
                not_found.append(existing_server_name)
        self.__check_servers__all = all_server_list
        self.__check_servers__list = servers_list
        self.__check_servers__not_found = not_found
        if found == len(servers_list):
            return True
        return False

    @property
    def all_servers_on_check(self):
        return self.__check_servers__all[:]

    @property
    def checked_servers_on_check(self):
        return self.__check_servers__list[:]

    @property
    def not_found_servers_on_check(self):
        return self.__check_servers__not_found[:]

    @staticmethod
    def __get_admin_device(device_info: str):
        return DeviceProxy("dserver/{}".format(device_info))

    def __get_server_host(self, instance_name) -> str:
        return self.__db.get_server_info(instance_name).host

    def __get_starter_device(self, instance_name: str) -> DeviceProxy:
        host_name = self.__get_server_host(instance_name).split(".")[0]
        return DeviceProxy("tango/admin/{}".format(host_name))
