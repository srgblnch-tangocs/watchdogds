#!/usr/bin/env python
# -*- coding:utf-8 -*-

# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016 CELLS/Alba synchrotron"
__license__ = "GPLv3+"
__status__ = "development"

__all__ = ["Watchdog", "WatchdogClass", "main"]
__docformat__ = 'restructuredtext'


from .dealer import build_dealer
from .dog import Dog, DEFAULT_RECHECK_TIME, SEPARATOR
import email.mime.text
import pprint
import smtplib
from socket import gethostname, getfqdn
import sys
import tango
from threading import Thread, Event, Lock
from time import ctime, sleep, time
import traceback


COLLECTION_REPORT_PERIOD = 3600*8  # every 8h, three times a day


# # Device States Description:
# INIT : during the events subscription and prepare.
# ON : when is normally running.
# ALARM : at least one sector can be too busy for the number of active cameras.
# FAULT : When something when wrong.


class Watchdog(tango.LatestDeviceImpl):
    #################################
    # # internal variables region ---
    _important_logs = None
    _all_devices = None
    # Done internal variables region ---
    ####################################

    ##################
    # # Logs region ---
    def clean_all_important_logs(self):
        # @todo: clean the important logs when they loose importance.
        self.debug_stream(
            "In {0}::clean_all_important_logs()".format(self.get_name()))
        self._important_logs = []
        self.add_status_message("")

    def add_status_message(self, current, important=False):
        self.debug_stream(
            "In {0}::add_status_message()".format(self.get_name()))
        msg_lst = ["The device is in {0} state.".format(self.get_state())]
        if self._important_logs is not None:
            msg_lst += self._important_logs
        msg_lst.append(current)
        status = '\n'.join(msg_lst)
        self.set_status(status)
        self.push_change_event('Status', status)
        if important and current not in self._important_logs:
            self._important_logs.append(current)

    def change_state(self, new_state):
        self.debug_stream(
            "In {0}::change_state({1})".format(self.get_name(), str(new_state)))
        self.set_state(new_state)
        self.push_change_event('State', new_state)
        self.clean_all_important_logs()
    # Done Logs region ---
    ######################

    #########################
    # # Process Properties ---
    def _process_deviceslist_property(self):
        """
        This method works with the raw input in the property DevicesList
        to convert it in the expected dictionary and do all the state
        event subscriptions.
        :return:
        """
        self.__parse_devicelist_property(self.DevicesList)
        self.__devicelist_property_to_attibute()
        self._extra_attributes = self.__prepare_extra_attributes()
        self.__extra_attributes_list_to_attribute()
        self.__build_extra_attributes()
        self.__build_dealer()

    def __parse_devicelist_property(self, property_obj):
        all_devices = []
        for i, line in enumerate(property_obj):
            fragments = line.split(',')
            for j, fragment in enumerate(fragments):
                if len(fragment) > 0:
                    try:
                        device_name = fragment.lower()
                        all_devices.append(device_name)
                    except Exception as e:
                        error_msg = \
                            "In {0}::__parse_devicelist_property() " \
                            "exception ({1} line, {2} element): {3}".format(
                                self.get_name(), i, j, str(e))
                        self.error_stream(error_msg)
                        traceback.print_exc()
        self._all_devices = all_devices

    def __devicelist_property_to_attibute(self):
        devicelist_attr = tango.SpectrumAttr('DevicesList',
                                             tango.DevString,
                                             tango.READ,
                                             1000)
        self.add_attribute(devicelist_attr, r_meth=self.read_DevicesList)

    def __extra_attributes_list_to_attribute(self):
        extra_attributes_attr = tango.SpectrumAttr('ExtraAttrList',
                                                   tango.DevString,
                                                   tango.READ,
                                                   1000)
        self.add_attribute(extra_attributes_attr, self.read_ExtraAttrList)

    def __build_extra_attributes(self):
        time_separation = self.__checktime_separation()
        extra_attributes_dct = {}
        for attribute_name in self._extra_attributes:
            extra_attributes_dct[attribute_name] = {'type': [],
                                                    'label': []}
        for i, device_name in enumerate(self._all_devices):
            try:
                self.__build_devicestate_attribute(device_name)
                for attribute_name in self._extra_attributes:
                    attribute_type, attribute_label = \
                        self.__build_device_extra_attribute(
                            device_name, attribute_name)
                    extra_attributes_dct[attribute_name]['type'].append(
                        attribute_type)
                    extra_attributes_dct[attribute_name]['label'].append(
                        attribute_label)
                start_delay = time_separation * i
                self.debug_stream(
                    "for {0} device {1} there will be a delay of {2:g} seconds"
                    "".format(i+1, device_name, start_delay))
                dog = Dog(device_name, self._joiner_event, start_delay,
                          self._extra_attributes, self)
                dog.try_fault_recovery = self.TryFaultRecover
                dog.try_hang_recovery = self.TryHangRecover
                self.DevicesDict[device_name] = dog
            except Exception as e:
                error_msg = "In {0}::__build_extra_attributes() exception in " \
                            "dogs generation: {1}:{2}: {3}".format(
                    self.get_name(), i, device_name, str(e))
                self.error_stream(error_msg)
                traceback.print_exc()
        for attribute_name in self._extra_attributes:
            attribute_types = extra_attributes_dct[attribute_name]['type']
            attribute_labels = extra_attributes_dct[attribute_name]['label']
            if len(attribute_types) != len(self._all_devices):
                self.error_stream(
                    "Not all the device succeed building the extra "
                    "attributes {0}".format(attribute_name))
            elif not all([True if each == attribute_types[0] else False
                          for each in attribute_types]):
                self.error_stream(
                    "Not all the devices have the same attribute type for "
                    "{0}: {1}".format(attribute_name, attribute_types))
            extra_attributes_lst = tango.SpectrumAttr(attribute_name,
                                                      attribute_types[0],
                                                      tango.READ,
                                                      1000)
            attribute_property = tango.UserDefaultAttrProp()
            attribute_property.set_label(attribute_labels[0])
            extra_attributes_lst.set_default_properties(attribute_property)
            self.add_attribute(extra_attributes_lst,
                               self.read_ExtraAttrLst)

    def __checktime_separation(self):
        """
        Given the list of devices to monitor, establish the time per device
        in order to maintain a global recheck period
        :return: time_separation
        """
        time_separation = DEFAULT_RECHECK_TIME/len(self._all_devices)
        self.debug_stream(
            "In {0}::__checktime_separation() for {1} devices: {2}".format(
                self.get_name(), len(self._all_devices), time_separation))
        return time_separation

    def __build_dealer(self):
        try:
            self.__build_dealer_attributes()
            self.__rebuild_dealer()
        except Exception as e:
            self.error_stream(
                "Exception building the dealer: {0}".format(str(e)))
            traceback.print_exc()
        else:
            self.debug_stream("Dealer build")

    def __build_dealer_attributes(self):
        # list of possible dealers
        dealer_options = tango.SpectrumAttr('Dealers', tango.DevString,
                                            tango.READ, 10)
        self.add_attribute(dealer_options, self.read_Dealers)
        # dealer in use (required to be memorised)
        dealer_attribute = tango.Attr('Dealer', tango.DevString,
                                      tango.READ_WRITE)
        dealer_attribute.set_memorized()
        dealer_attribute.set_memorized_init(True)
        self.add_attribute(dealer_attribute, self.read_Dealer,
                           self.write_Dealer)
        # TODO: dealer configurations (minimum, max, ...)

    def __rebuild_dealer(self, value='Equidistant'):
        dealer_lst = []  # Hackish!!!
        for dealer_attribute in self.DealerAttrList:
            fragments = dealer_attribute.split(',')
            for fragment in fragments:
                dealer_lst.append(fragment.strip())
        dogs_lst = self.DevicesDict.values()
        states_lst = [tango.DevState.RUNNING]
        if value == 'Equidistant':
            distance = [1000, 1000]
        elif value == 'MinMax':
            distance = [1000, 25000]
        else:
            return
        self._dealer = build_dealer(value, attributes_list=dealer_lst,
                                    dogs_list=dogs_lst, states_list=states_lst,
                                    distance=distance, parent=self)

    def __build_devicestate_attribute(self, device_name):
        dynamic_attribute_name = "{0}{1}State".format(
            device_name.replace("/", SEPARATOR), SEPARATOR)
        # Replace by an "impossible" symbol
        # --- FIXME: the separator would be improved
        attribute_property = tango.UserDefaultAttrProp()
        attribute_property.set_label("{0}/State".format(device_name))
        dynamic_attribute = tango.Attr(dynamic_attribute_name,
                                         tango.CmdArgType.DevState,
                                         tango.READ)
        dynamic_attribute.set_default_properties(attribute_property)
        self.add_attribute(dynamic_attribute,
                           r_meth=Watchdog.read_oneDeviceState,
                           is_allo_meth=Watchdog.is_oneDeviceState_allowed)
        self.set_change_event(dynamic_attribute_name, True, False)
        self.debug_stream(
            "In {0}::__build_devicestate_attribute() add dyn_attr {1}".format(
                self.get_name(), dynamic_attribute_name))

    def __prepare_extra_attributes(self):
        extra_attributes = []
        for i, elements in enumerate(self.ExtraAttrList):
            fragments = elements.split(',')
            for j, fragment in enumerate(fragments):
                if len(fragment) > 0:
                    try:
                        attribute_name = fragment.lower()
                        extra_attributes.append(attribute_name)
                    except Exception as e:
                        error_msg = \
                            "In {0}::__prepare_extra_attributes() exception " \
                            "in ExtraAttrList processing: (line {1}, " \
                            "fragment {2}: {3}): {4}".format(
                                self.get_name(), i, j, fragment, str(e))
                        self.error_stream(error_msg)
                        traceback.print_exc()
        return extra_attributes

    def __build_device_extra_attributes(self, device_name, attributes_list):
        for attribute_name in attributes_list:
            self.__build_device_extra_attribute(device_name, attribute_name)

    def __build_device_extra_attribute(self, device_name, attribute_name):
        full_attribute_name = "{0}/{1}".format(device_name, attribute_name)
        try:
            attribute_configuration = \
                tango.AttributeProxy(full_attribute_name).get_config()
            dynamic_attribute_name = full_attribute_name.replace('/', SEPARATOR)
            if attribute_configuration.data_format == \
                    tango.AttrDataFormat.SCALAR:
                attribute_property = tango.UserDefaultAttrProp()
                attribute_property.set_label(full_attribute_name)
                dynamic_attribute = tango.Attr(
                    dynamic_attribute_name, attribute_configuration.data_type,
                    attribute_configuration.writable)
                dynamic_attribute.set_default_properties(attribute_property)
                if attribute_configuration.writable == \
                        tango.AttrWriteType.READ_WRITE:
                    w_meth = Watchdog.write_ExtraAttribute
                else:
                    w_meth = None
                is_allowed = Watchdog.is_ExtraAttribute_allowed
                attribute_property = tango.UserDefaultAttrProp()
                attribute_property.set_label(
                    "{0}({1}".format(
                        full_attribute_name, attribute_configuration.label))
                dynamic_attribute.set_default_properties(attribute_property)
                self.add_attribute(dynamic_attribute,
                                   r_meth=Watchdog.read_ExtraAttribute,
                                   w_meth=w_meth,
                                   is_allo_meth=is_allowed)
                self.set_change_event(dynamic_attribute_name, True, False)
            else:
                raise Exception("Not yet supported array attributes")
            return (attribute_configuration.data_type,
                    attribute_configuration.label)
        except Exception as e:
            error_msg = "In {0}::__build_device_extra_attribute() exception " \
                        "in ExtraAttrList processing (attribute {1}): {2}" \
                        "".format(self.get_name(), full_attribute_name, str(e))
            self.error_stream(error_msg)
            traceback.print_exc()

    # Done Process Properties ---
    #############################

    ######################
    # # dyn_attr region ---
    def read_oneDeviceState(self, attr):
        # self.debug_stream(
        #     "In {0}::read_oneDeviceState()".format(self.get_name()))
        try:
            full_attribute_name = attr.get_name().replace(SEPARATOR, "/")
            device_name, _ = full_attribute_name.rsplit('/', 1)
        except Exception:
            self.error_stream(
                "In {0}::read_oneDeviceState() cannot extract the name "
                "from {1}".format(self.get_name(), attr.get_name()))
            attr.set_value(tango.DevState.UNKNOWN)
            return
        try:
            state = self.DevicesDict[device_name].device_state
            if not state:
                state = tango.DevState.UNKNOWN
            if state:
                attr.set_value(state)
            else:
                attr.set_value_date_quality(tango.DevState.UNKNOWN, time(),
                                            tango.AttrQuality.ATTR_INVALID)
        except Exception as e:
            self.error_stream(
                "In {0}::read_oneDeviceState() Exception with {1} reading "
                "the state: {2}".format(self.get_name(), device_name, str(e)))
            attr.set_value(tango.DevState.UNKNOWN)

    def is_oneDeviceState_allowed(self, req_type):
        if self.get_state() in [tango.DevState.FAULT]:
            return False
        return True

    def read_ExtraAttribute(self, attr):
        device_name, attribute_name = self._recover_device_attribute_name(attr)
        self.debug_stream(
            "In {0}::read_ExtraAttribute(): {1}/{2}".format(
                self.get_name(), device_name, attribute_name))
        try:
            value = self.DevicesDict[device_name].get_extra_attribute_value(
                attribute_name)
        except tango.DevFailed as e:
            if isinstance(e.args, tuple) and len(e.args) > 0:
                e = e.args[0]
                if isinstance(e, tango.DevError) and hasattr(e, 'desc'):
                    e = e.desc
            self.error_stream(
                "{0} read extra attribute DevFailed exception: {1}".format(
                    self.get_name(), str(e)))
            value = None
        except Exception as e:
            self.error_stream("{0} read extra attribute generic exception: {1}"
                              "".format(self.get_name(), e))
            value = None
        if value is not None:
            attr.set_value(value)
        else:
            # FIXME: improve the type check
            try:
                attr.set_value_date_quality(0, time(),
                                            tango.AttrQuality.ATTR_INVALID)
            except Exception:
                attr.set_value_date_quality("", time(),
                                            tango.AttrQuality.ATTR_INVALID)

    def write_ExtraAttribute(self, attr):
        device_name, attribute_name = self._recover_device_attribute_name(attr)
        data = []
        attr.get_write_value(data)
        value = data[0]
        self.debug_stream(
            "In {0}::write_ExtraAttribute(): {1}/{2} = {3}".format(
                self.get_name(), device_name, attribute_name, value))
        self.DevicesDict[device_name].set_extra_attribute_value(
            attribute_name, value)

    def is_ExtraAttribute_allowed(self, req_type):
        if self.get_state() in [tango.DevState.FAULT]:
            return False
        return True

    def _recover_device_attribute_name(self, attr):
        try:
            full_attribute_name = attr.get_name().replace(SEPARATOR, "/")
            device_name, attribute_name = full_attribute_name.rsplit('/', 1)
        except Exception:
            self.error_stream(
                "In {0}%s::_recover_device_attribute_name() cannot extract "
                "the name from {1}".format(self.get_name(), attr.get_name()))
            device_name, attribute_name = None, None
        return device_name, attribute_name

    def read_Dealer(self, attr):
        if not self._dealer:
            attr.set_value('')
        else:
            attr.set_value(self._dealer.type())

    def write_Dealer(self, attr=None):
        data = []
        attr.get_write_value(data)
        value = data[0]
        if self._dealer and value in self._dealer.types():
            self.__rebuild_dealer(value)
            self._dealer.distribute()

    def read_Dealers(self, attr):
        if not self._dealer:
            attr.set_value([''])
        else:
            attr.set_value(self._dealer.types())

    def read_DevicesList(self, attr):
        attr.set_value(self._all_devices)

    def read_ExtraAttrList(self, attr):
        attr.set_value(self._extra_attributes)

    def read_ExtraAttrLst(self, attr):
        attribute_name = attr.get_name()
        attribute_type = attr.get_data_type()
        values = []
        self.debug_stream("collecting {0}".format(attribute_name))
        for device_name in self._all_devices:
            self.debug_stream("{0}...".format(device_name))
            try:
                value = self.DevicesDict[device_name].get_extra_attribute_value(
                    attribute_name)
            except Exception as e:
                self.error_stream(
                    "{0} read extra attribute list DevFailed exception: {1}"
                    "".format(device_name, str(e)))
                value = None
            if value is None:
                self.debug_stream("{0}... None".format(device_name))
                if attribute_type in [tango.DevString]:
                    value = ''
                elif attribute_type in [tango.DevFloat, tango.DevDouble]:
                    value = float('nan')
                else:  # if attribute_type in [tango.DevShort, tango.DevLong]:
                    value = 0
            self.debug_stream("\tdevice {0}: {1}".format(device_name, value))
            values.append(value)
        attr.set_value(values)

    # TODO: more dynamic attributes:
    #       - allow to adjust the {Fault,Hand}Recovery for each of the watched
    #         (remember to make them memorised).
    #       - allow to adjust the recheck period for each of the watched
    #       - report threshold for running devices

    # Done dyn_attr region ---
    ##########################

    ####################
    # # events region ---
    def fire_events_list(self, events_attribute_sets):
        timestamp = time()
        attribute_names = []
        for attribute_event_set in events_attribute_sets:
            try:
                attribute_name = attribute_event_set[0]
                attribute_value = attribute_event_set[1]
                attribute_names.append(attribute_name)
                if len(attribute_event_set) == 3:  # specifies quality
                    attribute_quality = attribute_event_set[2]
                else:
                    attribute_quality = tango.AttrQuality.ATTR_VALID
                self.push_change_event(attribute_name, attribute_value,
                                       timestamp, attribute_quality)
                self.debug_stream(
                    "In {0}::fire_events_list() attribute: {1}, value: {2} "
                    "(quality {3}, timestamp {4})".format(
                        self.get_name(), attribute_name, attribute_value,
                        str(attribute_quality), str(timestamp)))
            except Exception as e:
                self.error_stream(
                    "In {0}::fire_events_list() Exception with attribute {1}:"
                    "\n{2}".format(
                        self.get_name(), attribute_event_set[0], str(e)))
        if len(attribute_names) > 0:
            self.debug_stream(
                "In {0}::fire_events_list() emitted {1} events: {2}".format(
                    self.get_name(), len(attribute_names), attribute_names))
    # Done events region ---
    ########################

    ################
    # Dog region ---
    def is_in_running_state_list(self, who):
        return self.is_in_list(self.attr_RunningDevicesList_read, who)

    def append_to_running(self, who):
        if self.append_to_list(self.attr_RunningDevicesList_read,
                               "Running", who):
            self.fire_in_running_state_attribute_events()
            if self._dealer:
                self._dealer.distribute()

    def remove_from_running(self, who):
        if self.remove_from_list(self.attr_RunningDevicesList_read,
                                 "Running", who):
            self.fire_in_running_state_attribute_events()
            if self._dealer:
                self._dealer.distribute()

    def fire_in_running_state_attribute_events(self):
        device_list = self.attr_RunningDevicesList_read[:]
        how_many_now = len(device_list)
        if self.attr_RunningDevices_read != how_many_now:
            self.attr_RunningDevices_read = how_many_now
            self._collect("RUNNING", how_many_now, device_list)
        self.fire_events_list([["RunningDevices", how_many_now],
                               ["RunningDevicesList", device_list]])

    def is_in_fault_state_list(self, who):
        return self.is_in_list(self.attr_FaultDevicesList_read, who)

    def append_to_fault(self, who):
        if self.append_to_list(self.attr_FaultDevicesList_read, "Fault", who):
            self.fire_in_fault_state_attribute_events()

    def remove_from_fault(self, who):
        if self.remove_from_list(self.attr_FaultDevicesList_read, "Fault", who):
            self.fire_in_fault_state_attribute_events()

    def fire_in_fault_state_attribute_events(self):
        device_lst = self.attr_FaultDevicesList_read[:]
        how_many_now = len(device_lst)
        if self.attr_FaultDevices_read != how_many_now:
            self.attr_FaultDevices_read = how_many_now
            self._report("FAULT", how_many_now, device_lst)
        self.fire_events_list([["FaultDevices", how_many_now],
                               ["FaultDevicesList", device_lst]])

    def is_in_hang_state_list(self, who):
        return self.is_in_list(self.attr_HangDevicesList_read, who)

    def append_to_hang(self, who):
        if self.append_to_list(self.attr_HangDevicesList_read, "Hang", who):
            self.fire_in_hang_state_attribute_events()

    def remove_from_hang(self, who):
        if self.remove_from_list(self.attr_HangDevicesList_read, "Hang", who):
            self.fire_in_hang_state_attribute_events()

    def fire_in_hang_state_attribute_events(self):
        device_list = self.attr_HangDevicesList_read[:]
        how_many_now = len(device_list)
        if self.attr_HangDevices_read != how_many_now:
            self.attr_HangDevices_read = how_many_now
            self._report("HANG", how_many_now, device_list)
        self.fire_events_list([["HangDevices", how_many_now],
                               ["HangDevicesList", device_list]])

    @staticmethod
    def is_in_list(lst, who):
        return lst.count(who)

    def append_to_list(self, lst, name, who):
        if not lst.count(who):
            lst.append(who)
            self.debug_stream("{0} append to {1} list".format(who, name))
            return True
        else:
            self.warn_stream(
                "{0} was already in the {1} list".format(who, name))
            return False

    def remove_from_list(self, lst, name, who):
        if lst.count(who):
            lst.pop(lst.index(who))
            self.info_stream("{0} removed from {1} list".format(who, name))
            return True
        else:
            self.warn_stream("{0} was NOT in the {1} list".format(who, name))
            return False

    def mailto(self, action, msg):
        if len(self.MailTo) != 0:
            name = self.get_name()  # .replace("/", SEPARATOR)
            mail = email.mime.text.MIMEText(msg)
            mail['From'] = "{0}@{1}".format(name, getfqdn())
            mail['To'] = ', '.join(self.MailTo)
            mail['Subject'] = "[{0}] {1}: {2}".format(
                self.__class__.__name__, self.get_name(), action)
            with smtplib.SMTP('localhost') as smtp:
                smtp.sendmail(mail['From'],  mail['To'], mail.as_string())

    def _report(self, action, how_many, list_obj):
        subject = "{0} report".format(action)
        mail_body = [
            "Status Report from the watchdog {0}\n".format(self.get_name()),
            "{0} devices change to {1}\n{2}".format(
                action, how_many, list_obj),
            "\n--\nEnd transmission."]
        self.mailto(subject, ''.join(mail_body))
        self._collect(action, how_many, list_obj)

    def _prepare_collector_thread(self):
        if self._changes_collector is None:
            try:
                # h to s
                self.__report_period_in_seconds = int(self.ReportPeriod)*60*60
            except Exception:
                self.__report_period_in_seconds = COLLECTION_REPORT_PERIOD
            self.debug_stream(
                "Setting the periodic report period to {0}h".format(
                    self.__report_period_in_seconds/60/60))
            self._last_collection = time()
            self._changes_collector = Thread(target=self._collection_reporter)
            self._changes_collector.setDaemon(True)
            self._changes_collector.start()
            self.debug_stream("Collector thread launched")

    def _collect(self, action, how_many, list_obj):
        try:
            with self._changes_collector_lock:
                if action not in self._changes_dict:
                    self._changes_dict[action] = {}
                now = ctime()
                while now in self._changes_dict[action]:
                    now += "."  # if many in the same second, tag them
                self._changes_dict[action][now] = [how_many, list_obj]
            self.debug_stream(
                "Collected {0} information: {1} and {2}".format(
                    action, how_many, list_obj))
        except Exception as e:
            self.error_stream(
                "Exception collecting {0} information: {1}".format(
                    action, str(e)))

    def _collection_reporter(self):
        self.info_stream(
            "Collector thread says hello. First report in {0} seconds".format(
                self.__report_period_in_seconds))
        sleep(self.__report_period_in_seconds)
        while not self._joiner_event.is_set():
            t0 = time()
            self.debug_stream("Collector thread starts reporting process")
            if self._do_collector_report():
                self._changes_dict = {}
            t_diff = time()-t0
            if not self._joiner_event.is_set():
                _next = self.__report_period_in_seconds-t_diff
                self.debug_stream("Next report in {0} seconds".format(_next))
                sleep(_next)

    def _do_collector_report(self):
        # todo: information about the image counter
        subject = "Watchdog periodic report"
        try:
            avoid_send = True  # do not send if there is nothing to send
            mail_body = [
                "Status Report since {0} of watchdog {1}\n\n".format(
                    ctime(self._last_collection), self.get_name())]
            with self._changes_collector_lock:
                if len(self._changes_dict.keys()) > 0:
                    avoid_send = False  # there is something to be reported
                    for action in list(self._changes_dict.keys()):
                        mail_body.append(
                            "Collected events for action {0}\n".format(action))
                        when_list = list(self._changes_dict[action].keys())
                        when_list.sort()
                        for when in when_list:
                            how_many, list_obj = \
                                self._changes_dict[action][when]
                            mail_body.append(
                                "\tat {0}: {1} devices\n".format(
                                    when, how_many))
                            list_obj.sort()
                            for each in list_obj:
                                mail_body.append("\t\t{0}\n".format(each))
                    self._last_collection = time()
            mail_body.append("\n--\nEnd transmission.")
            if not avoid_send:
                self.mailto(subject, ''.join(mail_body))
            return True
        except Exception as e:
            self.error_stream(
                "Exception reporting collected information: {0}".format(
                    str(e)))
            traceback.print_exc()
            try:
                subject = "Report exception"
                mail_body = ""
                self.mailto(subject, mail_body)
            except Exception as e2:
                self.error_stream(
                    "Not reported the first exception, because another in "
                    "email send: {0}".format(str(e2)))
                traceback.print_exc()
            return False
    # Done dog region ---
    #####################

    def __init__(self, cl, name):
        tango.LatestDeviceImpl.__init__(self, cl, name)
        self.debug_stream("In __init__()")
        # --- set the vbles
        self.DevicesDict = {}
        self.attr_RunningDevices_read = 0
        self.attr_RunningDevicesList_read = []
        self.attr_FaultDevices_read = 0
        self.attr_FaultDevicesList_read = []
        self.attr_HangDevices_read = 0
        self.attr_HangDevicesList_read = []
        # --- tools for the Exec() cmd
        DS_MODULE = __import__(self.__class__.__module__)
        kM = dir(DS_MODULE)
        vM = map(DS_MODULE.__getattribute__, kM)
        self.__globals = dict(zip(kM, vM))
        self.__globals['self'] = self
        self.__globals['module'] = DS_MODULE
        self.__locals = {}
        # --- process properties
        self._joiner_event = Event()
        self._joiner_event.clear()
        self._changes_collector = None
        self._changes_dict = {}
        self._changes_collector_lock = Lock()
        self.__report_period_in_seconds = None
        self._prepare_collector_thread()
        self._dealer = None
        self._all_devices = []
        # tango init
        Watchdog.init_device(self)

    def delete_device(self):
        self.debug_stream("In delete_device()")
        self._joiner_event.set()
        # TODO: check the stop process for each of the dogs

    def init_device(self):
        self.debug_stream("In init_device()")
        self.get_device_properties(self.get_device_class())
        # --- prepare attributes that will have events ----
        self.set_change_event('State', True, False)
        self.set_change_event('Status', True, False)
        self.change_state(tango.DevState.INIT)
        self.set_change_event('RunningDevices', True, False)
        self.set_change_event('RunningDevicesList', True, False)
        self.set_change_event('FaultDevices', True, False)
        self.set_change_event('FaultDevicesList', True, False)
        self.set_change_event('HangDevices', True, False)
        self.set_change_event('HangDevicesList', True, False)
        self._process_deviceslist_property()
        # everything ok:
        self.change_state(tango.DevState.ON)

    def always_executed_hook(self):
        # self.debug_stream("In always_excuted_hook()")
        pass

    # -------------------------------------------------------------------------
    #    Watchdog read/write attribute methods
    # -------------------------------------------------------------------------
    def read_RunningDevices(self, attr):
        # self.debug_stream("In read_RunningDevices()")
        attr.set_value(self.attr_RunningDevices_read)

    def read_FaultDevices(self, attr):
        # self.debug_stream("In read_FaultDevices()")
        attr.set_value(self.attr_FaultDevices_read)

    def read_HangDevices(self, attr):
        # self.debug_stream("In read_HangDevices()")
        attr.set_value(self.attr_HangDevices_read)

    def read_RunningDevicesList(self, attr):
        # self.debug_stream("In read_RunningDevicesList()")
        attr.set_value(self.attr_RunningDevicesList_read)

    def read_FaultDevicesList(self, attr):
        # self.debug_stream("In read_FaultDevicesList()")
        attr.set_value(self.attr_FaultDevicesList_read)

    def read_HangDevicesList(self, attr):
        # self.debug_stream("In read_HangDevicesList()")
        attr.set_value(self.attr_HangDevicesList_read)

    def initialize_dynamic_attributes(self):
        pass

    def read_attr_hardware(self, data):
        # self.debug_stream("In read_attr_hardware()")
        pass


# =============================================================================
#    Watchdog command methods
# =============================================================================

# -----------------------------------------------------------------------------
#    Exec command:
# -----------------------------------------------------------------------------
    def Exec(self, argin):
        """ Hackish expert attribute to look inside the device during
        execution. If you use it, be very careful and at your own risk.
        :param argin:
        :type: tango.DevString
        :return: argout:
        :rtype: tango.DevString """
        self.debug_stream("In {0}.Exec()".format(self.get_name()))
        argout = ''
        try:
            try:
                # interpretation as expression
                argout = eval(argin, self.__globals, self.__locals)
            except SyntaxError:
                # interpretation as statement
                exec(argin, self.__globals, self.__locals)
                argout = self.__locals.get("y")
        except Exception as exc:
            # handles errors on both eval and exec level
            argout = traceback.format_exc()
        if isinstance(argout, str):
            return argout
        elif isinstance(argout, BaseException):
            return "{0}!\n{1}".format(argout.__class__.__name__, str(argout))
        else:
            try:
                return pprint.pformat(argout)
            except Exception:
                return str(argout)
        return argout


class WatchdogClass(tango.DeviceClass):
    def dyn_attr(self, dev_list):
        """Invoked to create dynamic attributes for the given devices.
        Default implementation calls
        :meth:`Watchdog.initialize_dynamic_attributes` for each device
        :param dev_list: list of devices
        :type dev_list: :class:`tango.DeviceImpl`"""
        for dev in dev_list:
            try:
                dev.initialize_dynamic_attributes()
            except Exception:
                import traceback
                dev.warn_stream("Failed to initialize dynamic attributes")
                dev.debug_stream("Details: {0}".format(traceback.format_exc()))

    #    Class Properties
    class_property_list = {
        }

    #    Device Properties
    device_property_list = {
        'DevicesList':
            [tango.DevVarStringArray,
             "List of string with the name of the devices to be watched.",
             []],
        'ExtraAttrList':
            [tango.DevVarStringArray,
             "For each of the devices watched, if exist, build attribute "
             "mirrors ",
             []],
        'DealerAttrList':
            [tango.DevVarStringArray,
             "Attributes that the dealer will balance",
             []],
        # 'DeviceGroups':
        #     [tango.DevVarStringArray,
        #      "Dictionary with list in the items similar to the DeviceList "
        #      "but classifying the devices in groups using the keys.",
        #      []],
        'TryFaultRecover':
            [tango.DevBoolean,
             "Flag to tell the device that, if possible, try to recover "
             "cameras in fault state",
             []],
        'TryHangRecover':
            [tango.DevBoolean,
             "Flag to tell the device that, if possible, try to recover hang "
             "cameras",
             []],
        'MailTo':
            [tango.DevVarStringArray,
             "List of mail destinations to report when fault or hang lists "
             "changes",
             []],
        'ReportPeriod':
            [tango.DevUShort,
             "Hours between periodic reports",
             []]
        }

    #    Command definitions
    cmd_list = {
        'Exec':
            [[tango.DevString, "none"],
             [tango.DevString, "none"],
             {'Display level': tango.DispLevel.EXPERT}],
        }

    #    Attribute definitions
    attr_list = {
        'RunningDevices':
            [[tango.DevUShort,
              tango.SCALAR,
              tango.READ],
             {'description': "Number of ccd running state in total."}],
        'FaultDevices':
            [[tango.DevUShort,
              tango.SCALAR,
              tango.READ],
             {'description': "Number of ccd in fault state in total."}],
        'HangDevices':
            [[tango.DevUShort,
              tango.SCALAR,
              tango.READ],
             {'description': "Number of ccd non accessible in total."}],
        'RunningDevicesList':
            [[tango.DevString,
              tango.SPECTRUM,
              tango.READ, 999]],
        'FaultDevicesList':
            [[tango.DevString,
              tango.SPECTRUM,
              tango.READ, 999]],
        'HangDevicesList':
            [[tango.DevString,
              tango.SPECTRUM,
              tango.READ, 999]],
        }


def main():
    try:
        py = tango.Util(sys.argv)
        py.add_class(WatchdogClass, Watchdog, 'Watchdog')

        U = tango.Util.instance()
        U.server_init()
        U.server_run()

    except tango.DevFailed as e:
        print("-------> Received a DevFailed exception: {0}".format(str(e)))
    except Exception as e:
        print("-------> An unforeseen exception occured.... {0}".format(str(e)))


if __name__ == '__main__':
    main()
