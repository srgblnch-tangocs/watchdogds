# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016 CELLS/Alba synchrotron"
__license__ = "GPLv3+"
__status__ = "development"

from .dog import Logger, Dog
from numpy import linspace
from sys import version_info
from tango import DevState


class Dealer(Logger):
    __attribute_list = None
    __dogs = None
    __states = None

    def __init__(self, attributes_list, dogs_list, states_list=None, minimum=0,
                 *args, **kwargs):
        if version_info.major >= 3:
            super(Dealer, self).__init__(*args, **kwargs)
        else:
            Logger.__init__(self, *args, **kwargs)
        self._set_dogs_list(dogs_list)
        self._set_states_list(states_list)
        self._set_attributes_list(attributes_list)
        self.minimum = minimum

    def type(self):
        if type(self) == Equidistant:
            return 'Equidistant'
        if type(self) == MinMax:
            return 'MinMax'
        return ''

    @staticmethod
    def types():
        return ['Equidistant', 'MinMax']

    def _set_dogs_list(self, dogs_list):
        if all([isinstance(dog, Dog) for dog in dogs_list]):
            self.__dogs = dogs_list
        else:
            raise AssertionError(
                "Cannot manage the list of dogs {0}".format(dogs_list))

    def _set_states_list(self, states_list):
        if states_list is None:
            self.__states = [DevState.RUNNING]
        elif isinstance(states_list, list) and\
                all([x in DevState.values.values() for x in states_list]):
            self.__states = states_list
        else:
            raise AssertionError(
                "Cannot manage the list of states {0}".format(states_list))

    def _set_attributes_list(self, attributes_list):
        if isinstance(attributes_list, list):
            self.__attributes_list = []
            for attribute_name in attributes_list:
                if all([dog.has_extra_attributes(attribute_name)
                        for dog in self.__dogs]):
                    self.__attributes_list.append(attribute_name)
                else:
                    raise AssertionError(
                        "Not all the dogs have the attribute {0}".format(
                            attribute_name))
        else:
            raise AssertionError("attributes_list must be a list")

    @property
    def minimum(self):
        return self.__minimum

    @minimum.setter
    def minimum(self, value):
        if isinstance(value, int):
            self.__minimum = value
        else:
            raise TypeError("Must be an integer")

    @property
    def attributes_list(self):
        return self.__attributes_list[:]

    @property
    def dogs_list(self):
        return self.__dogs[:]

    def pair_of_attributes(self):
        if len(self.__attributes_list) == 0:
            self.error_stream(
                "No attributes to distribute. Check 'DealerAttrList' property!")
        i = 0
        while i < len(self.__attributes_list):
            first = self.__attributes_list[i]
            if i+1 < len(self.__attributes_list):
                second = self.__attributes_list[i+1]
            else:
                second = None
            self.info_stream(
                "Requested {0:d}th pair of attributes: {1}".format(
                    i, [first, second]))
            yield [first, second]
            i += 2

    @property
    def attribute_values(self):
        answer = {}
        for dog in self.__dogs:
            answer[dog.device_name] = [dog.get_extra_attribute_value(
                attribute_name) for attribute_name in self.__attribute_list]
        return answer

    @property
    def device_states(self):
        answer = {}
        for dog in self.__dogs:
            answer[dog.device_name] = dog.device_state
        return answer

    @property
    def candidates(self):
        candidates = []
        for dog in self.__dogs:
            if dog.device_state in self.__states:
                candidates.append(dog)
        self.info_stream("Dealer candidates: {0}".format(candidates))
        return candidates

    def distribute(self):
        raise NotImplementedError("No super class implementation")

    def do_write(self, dog, attribute_name, value):
        try:
            dog.set_extra_attribute_value(attribute_name, value)
        except Exception as e:
            self.error_stream(e)


class Equidistant(Dealer):
    __distance = None

    def __init__(self, distance, *args, **kwargs):
        if version_info.major >= 3:
            super(Equidistant, self).__init__(*args, **kwargs)
        else:
            Dealer.__init__(self, *args, **kwargs)
        self.distance = distance

    @property
    def distance(self):
        return self.__distance

    @distance.setter
    def distance(self, value):
        if isinstance(value, int):
            self.__distance = value
        else:
            raise TypeError("'distance' must be an integer")

    def distribute(self):
        candidates = self.candidates
        if len(candidates) == 0:
            return
        distribution = range(self.minimum,
                             self.minimum+(len(candidates)*self.distance),
                             self.distance)
        _reversed = list(distribution)
        _reversed.reverse()
        self.info_stream("Dealer distribution: {0}".format(distribution))
        for i, dog in enumerate(candidates):
            self.info_stream(
                "Working for candidate {0}: {1}".format(i, dog.device_name))
            for first, second in self.pair_of_attributes():
                self.do_write(dog, first, distribution[i])
                self.do_write(dog, second, _reversed[i])


class MinMax(Dealer):
    __maximum = None

    def __init__(self, maximum, *args, **kwargs):
        if version_info.major >= 3:
            super(MinMax, self).__init__(*args, **kwargs)
        else:
            Dealer.__init__(self, *args, **kwargs)
        self.maximum = maximum

    @property
    def maximum(self):
        return self.__maximum

    @maximum.setter
    def maximum(self, value):
        if isinstance(value, int):
            self.__maximum = value
        else:
            raise TypeError("'maximum' must be an integer")

    def distribute(self):
        candidates = self.candidates
        distribution = linspace(self.minimum, self.maximum, len(candidates))
        self.info_stream("Dealer distribution: {0}".format(distribution))
        for i, dog in enumerate(candidates):
            self.info_stream(
                "Working for candidate {0}: {1}".format(i, dog.device_name))
            for first, second in self.pair_of_attributes():
                self.do_write(dog, first, int(distribution[i]))
                self.do_write(dog, second, int(distribution[-i]))


def build_equidistant_dealer(attributes_list, dogs_list, states_list,
                             distance, parent):
    return Equidistant(attributes_list=attributes_list, dogs_list=dogs_list,
                       states_list=states_list, minimum=distance[0],
                       distance=distance[1], parent=parent)


def build_minmax_dealer(attributes_list, dogs_list, states_list,
                        distance, parent):
    return MinMax(attributes_list=attributes_list, dogs_list=dogs_list,
                  states_list=states_list, minimum=distance[0],
                  maximum=distance[1], parent=parent)


def build_dealer(dealer_type, attributes_list, dogs_list, states_list,
                 distance, parent):
    if dealer_type == 'Equidistant':
        return build_equidistant_dealer(attributes_list, dogs_list,
                                        states_list, distance, parent)
    if dealer_type == 'MinMax':
        return build_minmax_dealer(attributes_list, dogs_list, states_list,
                                   distance, parent)
