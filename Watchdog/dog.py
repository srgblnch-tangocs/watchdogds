# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 3
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

__author__ = "Sergi Blanch-Torne"
__email__ = "sblanch@cells.es"
__copyright__ = "Copyright 2016 CELLS/Alba synchrotron"
__license__ = "GPLv3+"
__status__ = "development"

__all__ = ["Logger", "Dog", "WatchdogTester", "main",
           "DEFAULT_RECHECK_TIME", "SEPARATOR"]
__docformat__ = 'restructuredtext'

try:
    from fandango import Astor  # soft dependency
except ImportError:
    from .astor import Astor
import email.mime.text
import smtplib
from socket import gethostname
from sys import version_info
from tango import DeviceProxy, DevState, EventType, DevFailed, DevError
from time import sleep, time
from threading import Thread, Event
import traceback


DEFAULT_RECHECK_TIME = 90.0  # seconds
DEFAULT_nOVERLAPS_ALERT = 10
DEFAULT_ASTOR_nSTOPS = 2
DEFAULT_ASTOR_STOPWAIT = 3  # seconds
SEPARATOR = "\\"


class Logger:
    def __init__(self, parent, *args, **kwargs):
        if version_info.major >= 3:
            super(Logger, self).__init__(*args, **kwargs)
        else:
            pass
        self._parent = parent
        # --- tango streams
        self.error_stream = parent.error_stream
        self.warn_stream = parent.warn_stream
        self.info_stream = parent.info_stream
        self.debug_stream = parent.debug_stream
        # --- tango event retransmission
        self.fire_events_list = parent.fire_events_list
        # --- running
        self.is_in_running_state_list = parent.is_in_running_state_list
        self.append_to_running = parent.append_to_running
        self.remove_from_running = parent.remove_from_running
        # --- fault
        self.is_in_fault_state_list = parent.is_in_fault_state_list
        self.append_to_fault = parent.append_to_fault
        self.remove_from_fault = parent.remove_from_fault
        # --- hang
        self.is_in_hang_state_list = parent.is_in_hang_state_list
        self.append_to_hang = parent.append_to_hang
        self.remove_from_hang = parent.remove_from_hang
        # --- mailto
        self.mailto = parent.mailto

    @property
    def device_name(self):
        raise NotImplemented

    def fire_event(self, attribute_name, value, timestamp=None, quality=None):
        full_attribute_name = ''.join([self.device_name.replace("/", SEPARATOR),
                                       SEPARATOR, attribute_name])
        try:
            if timestamp and quality:
                self.fire_events_list(
                    [[full_attribute_name, value, timestamp, quality]])
            else:
                self.fire_events_list([[full_attribute_name, value]])
        except Exception as e:
            self.error_stream(
                "Cannot fire event for {0}/{1}: {2}".format(
                    self.device_name, attribute_name, e))
            traceback.print_exc()


class Dog(Logger):
    _device_name = None
    _device_proxy = None
    _event_id = None
    _device_state = None
    _try_fault_recovery = None
    _fault_recovery_ctr = None
    _device_status = None
    _try_hang_recovery = None
    _hang_recovery_ctr = None
    _joiner_event = None
    _thread = None
    _recheck_period = None
    _overlaps = None
    _overlaps_alert = None
    _extra_attributes = None
    _extra_event_ids = None
    _extra_attribute_values = None

    def __init__(self, device_name, joiner_event=None, start_delay=None,
                 extra_attributes=None, *args, **kwargs):
        if version_info.major >= 3:
            super(Dog, self).__init__(*args, **kwargs)
        else:
            Logger.__init__(self, *args, **kwargs)
        self._device_name = device_name
        # --- fault variables
        self._try_fault_recovery = False
        self._fault_recovery_ctr = 0
        # --- hang variables
        self._try_hang_recovery = False
        self._hang_recovery_ctr = 0
        # --- Thread for hang monitoring
        self._joiner_event = joiner_event
        self._recheck_period = DEFAULT_RECHECK_TIME
        self._overlaps = 0
        self._overlaps_alert = DEFAULT_nOVERLAPS_ALERT
        # --- extra attributes
        self._extra_attributes = []
        self._extra_event_ids = {}
        self._extra_attribute_values = {}
        if extra_attributes is not None:
            for attribute_name in extra_attributes:
                attribute_name = attribute_name.lower()
                self._extra_attributes.append(attribute_name)
                self._extra_event_ids[attribute_name] = None
                self._extra_attribute_values[attribute_name] = None
        # --- build proxy and event subscriptions
        self.__build_proxy()
        self.__create_thread(start_delay)

    def __str__(self):
        return "Dog({0}, state={1})".format(self.device_name, self.device_state)

    def __repr__(self):
        return "Dog({0}, state={1}, fault_recovery={2}, hang_recovery={3})" \
               "".format(self.device_name, self.device_state,
                         self.try_fault_recovery, self.try_hang_recovery)

    # --- object properties

    @property
    def device_name(self):
        return self._device_name

    @property
    def device_proxy(self):
        return self._device_proxy

    @property
    def device_state(self):
        return self._device_state

    def has_extra_attributes(self, attribute_name):
        return self._extra_attributes.count(attribute_name.lower()) > 0

    def get_extra_attribute_value(self, attribute_name):
        try:
            value = self._device_proxy[attribute_name].value
            timestamp = self._device_proxy[attribute_name].time.totime()
            quality = self._device_proxy[attribute_name].quality
            if value != self._extra_attribute_values[attribute_name]:
                self.debug_stream(
                    "{0}/{1} has changed from {2} to {3}".format(
                        self.device_name, attribute_name,
                        self._extra_attribute_values[attribute_name], value))
                self._extra_attribute_values[attribute_name] = value
                self.fire_event(attribute_name, value, timestamp, quality)
            if self.is_in_hang_state_list(self.device_name):
                self.remove_from_hang(self.device_name)
            return value
        except DevFailed as e:
            if not self.is_in_hang_state_list(self.device_name):
                try:
                    self.device_proxy.State()
                except Exception:
                    self.append_to_hang(self.device_name)
                if e.args[0].reason in ['ATTRIBUTE_UNAVAILABLE',
                                        'SOFTWARE_FAILURE']:
                    return
                self.warn_stream(
                    "{0}/{1} read exception: {2} {3}".format(
                        self.device_name, attribute_name,
                        e.args[0].reason, e.args[0].desc))
        except Exception as e:
            self.error_stream(
                "{0}/{1} read exception: {2}".format(
                    self.device_name, attribute_name, str(e)))
        raise Exception(
            "{0}/{1} cannot be read".format(self.device_name, attribute_name))

    def set_extra_attribute_value(self, attribute_name, value):
        try:
            self.info_stream(
                "Writing {0}/{1} with {2}".format(
                    self.device_name, attribute_name, str(value)))
            self._device_proxy[attribute_name] = value
            if self.is_in_hang_state_list(self.device_name):
                self.remove_from_hang(self.device_name)
        except DevFailed as main_exception:
            if not self.is_in_hang_state_list(self.device_name):
                try:
                    self.device_proxy.State()
                except Exception as inner_exception:
                    # Canary message to know the exception type and specify
                    self.debug_stream(
                        "Exception {0} while try to read device state".format(
                            type(inner_exception)))
                    self.append_to_hang(self.device_name)
                if isinstance(main_exception, list) and \
                        len(main_exception) >= 1:
                    first_exception = main_exception[0]
                    if hasattr(first_exception, 'reason') and \
                            hasattr(first_exception, 'desc'):
                        exception_msg = "{0} {1}".format(
                            first_exception.reason, first_exception.desc)
                    else:
                        exception_msg = str(first_exception)
                else:
                    exception_msg = str(main_exception)
                self.warn_stream(
                    "{0}/{1} write exception: {2}".format(
                        self.device_name, attribute_name, exception_msg))
        except Exception as main_exception:
            self.error_stream(
                "{0}/{1} write exception ({2}): {3}".format(
                    self.device_name, attribute_name, type(main_exception),
                    str(main_exception)))
            raise Exception(
                "{0}/{1} cannot be write".format(
                    self.device_name, attribute_name))

    @property
    def try_fault_recovery(self):
        return self._try_fault_recovery

    @try_fault_recovery.setter
    def try_fault_recovery(self, value):
        if isinstance(value, bool):
            self._try_fault_recovery = value
        else:
            raise TypeError("Only boolean assignment")

    @property
    def try_hang_recovery(self):
        return self._try_hang_recovery

    @try_hang_recovery.setter
    def try_hang_recovery(self, value):
        if isinstance(value, bool):
            # if value and not Astor:
            #     # Only check Astor if trying to set to True
            #     self.error_stream("This feature is only available with "
            #                       "fandango's Astor present")
            #     return
            self._try_hang_recovery = value
        else:
            raise TypeError("Only boolean assignment")

    @property
    def recheck_period(self):
        return self._recheck_period

    @property
    def overlaps_alert(self):
        return self._overlaps_alert

    # --- Constructor methods

    def __build_proxy(self):
        try:
            self._device_proxy = DeviceProxy(self._device_name)
        except DevFailed as e:
            self.error_stream(
                "{0} proxy build exception: {1}".format(
                    self.device_name, self.__process_devfailed_exception(e)))
            self.__build_failed()
        except Exception as e:
            self.error_stream(
                "{0} proxy not available: {1}".format(
                    self.device_name, str(e)))
            traceback.print_exc()
            self.__build_failed()
        else:
            try:
                self.__subscribe_event()
            except DevFailed as e:
                self.error_stream(
                    "{0} subscribe to events failed: {1}".format(
                        self.device_name,
                        self.__process_devfailed_exception(e)))
            except Exception as e:
                self.error_stream(
                    "{0} subscribe to events exception ({1}): {2}".format(
                        self.device_name, type(e), str(e)))

    @staticmethod
    def __process_devfailed_exception(exception_obj: DevFailed) -> str:
        if isinstance(exception_obj.args, tuple) \
                and len(exception_obj.args) > 0:
            exception_obj = exception_obj.args[0]
            if isinstance(exception_obj, DevError) \
                    and hasattr(exception_obj, 'desc'):
                exception_obj = exception_obj.desc
        return str(exception_obj)

    def __build_failed(self):
        self.append_to_hang(self.device_name)

    def __subscribe_event(self):
        if self._device_proxy is None:
            return
        self._event_id = \
            self._device_proxy.subscribe_event('State',
                                               EventType.CHANGE_EVENT,
                                               self)
        self.debug_stream(
            "Subscribed to {0}/State (id={1})".format(
                self.device_name, self._event_id))
        self.__subscribe_extra_attributess()

    def __unsubscribe_event(self):
        if self._event_id:
            try:
                self._device_proxy.unsubscribe_event(self._event_id)
            except Exception as exception:
                # Canary with the type of the exception to specify
                self.error_stream(
                    "{0} failed to unsubscribe event ({1}): {2}".format(
                        self.device_name, type(exception), str(exception)))
            self._event_id = None
        else:
            self.warn_stream(
                "{0} no event id to unsubscribe.".format(self.device_name))
        self.__unsubscribe_extra_attributes()

    def __subscribe_extra_attributess(self):
        for attribute_name in self._extra_attributes:
            try:
                self._extra_event_ids[attribute_name] = \
                    self._device_proxy.subscribe_event(attribute_name,
                                                       EventType.CHANGE_EVENT,
                                                       self)
                self.debug_stream(
                    "Subscribed to {0}/{1} (id={2})".format(
                        self.device_name, attribute_name,
                        self._extra_event_ids[attribute_name]))
            except DevFailed as exception:
                self.warn_stream(
                    "{0}/{1} failed to subscribe event: {2}".format(
                        self.device_name, attribute_name,
                        self.__process_devfailed_exception(exception)))
            except Exception as exception_obj:
                # Canary in the message to know an specialize
                self.error_stream(
                    "{0}/{1} failed to subscribe event ({2}): {3}".format(
                        self.device_name, attribute_name, type(exception_obj),
                        str(exception_obj)))

    def __unsubscribe_extra_attributes(self):
        for attribute_name in self._extra_event_ids.keys():
            if self._extra_event_ids[attribute_name]:
                try:
                    self._device_proxy.unsubscribe_event(
                        self._extra_event_ids[attribute_name])
                except Exception as exception_obj:
                    # Canary to know and specialize
                    self.error_stream(
                        "{0}/{1} failed to unsubscribe event ({2}): {3}"
                        "".format(self.device_name, attribute_name,
                                  type(exception_obj), str(exception_obj)))
                self._extra_event_ids[attribute_name] = None
            else:
                self.warn_stream(
                    "{0}/{1} no event id to unsubscribe.".format(
                        self.device_name, attribute_name))

    def __create_thread(self, start_delay):
        try:
            self._thread = Thread(target=self.__hang_monitor_thread,
                                  args=(start_delay,))
            self._thread.setDaemon(True)
            if start_delay > 0:
                self.info_stream(
                    "Monitor {0} will wait {1:g} seconds until thread "
                    "start".format(self.device_name, start_delay))
            else:
                self.info_stream(
                    "Monitor {0} will start the thread immediately".format(
                        self.device_name))
            self._thread.start()
        except Exception as exception:
            # Canary to know and specialize
            self.error_stream(
                "{0} hang monitor thread creation fail ({1}): {2}".format(
                    self.device_name, type(exception), str(exception)))
            traceback.print_exc()

    # --- Events
    def push_event(self, event):
        try:
            if event is None:
                return
            if not hasattr(event, 'attr_value') or event.attr_value is None \
                    or event.attr_value.value is None:
                self.debug_stream(
                    "{0} push_event() {1}: value has None type".format(
                        self.device_name, event.attr_name))
                return
            # self.info_stream("{0}: {1} = {2}".format(self.device_name,
            #                                          event.attr_value.name,
            #                                          event.attr_value.value))
            try:  # ---FIXME: Ugly!! but it comes with a fullname
                # name_split = event.attr_name.split('/', 3)
                # it may start as tango://...
                name_split = event.attr_name.rsplit('/', 4)[-4:]
                domain, family, member, attribute_name = name_split
            except Exception as exception:
                # Canary to know and specialize
                self.error_stream(
                    "{0} push_event() error splitting the attr_name {1} from "
                    "the event. ({2})".format(
                        self.device_name, event.attr_name, type(exception)))
                return
            device_name = '/'.join([domain, family, member])
            attribute_name = attribute_name.lower()
            if device_name != self.device_name:
                self.error_stream(
                    "Event received doesn't correspond with who the listener "
                    "expects ({0} != {1})".format(
                        device_name, self.device_name))
                return
            # ---
            if attribute_name == 'state':
                self.debug_stream(
                    "{0} push_event() value = {1}".format(
                        self.device_name, event.attr_value.value))
                self.__check_device_state(event.attr_value.value)
                self.fire_event('State', event.attr_value.value)
            elif attribute_name in self._extra_attributes:
                self.debug_stream(
                    "{0}/{1} push_event() value = {2}".format(
                        self.device_name, attribute_name,
                        event.attr_value.value))
                self.fire_event(attribute_name, event.attr_value.value)
                self._extra_attribute_values[attribute_name] = \
                    event.attr_value.value
            else:
                self.warn_stream(
                    "{0}/{1} push_event() unmanaged attribute "
                    "(value = {2})".format(self.device_name, attribute_name,
                                           event.attr_value.value))
        except Exception as e:
            self.debug_stream(
                "{0} push_event() Exception {1}".format(
                    self.device_name, str(e)))
            traceback.print_exc()

    # --- checks

    def __check_device_state(self, new_state=None):
        if self.__state_has_changed(new_state):
            self.info_stream(
                "{0} state change from {1} to {2}".format(
                    self.device_name, self._device_state, new_state))
            old_state = self._device_state
            self._device_state = new_state
            try:
                # state change to one of the lists
                if new_state is DevState.RUNNING:
                    self.append_to_running(self.device_name)
                elif new_state is DevState.FAULT:
                    self.append_to_fault(self.device_name)
                # state change from one of the lists
                elif self.__was_running() or \
                        self.is_in_running_state_list(self.device_name):
                    self.remove_from_running(self.device_name)
                elif self.__was_in_fault() or \
                        self.is_in_fault_state_list(self.device_name):
                    self.remove_from_fault(self.device_name)
                    self._fault_recovery_ctr = 0
                # recover from Hang
                if self.device_state is None or \
                        self.is_in_hang_state_list(self.device_name):
                    self.debug_stream(
                        "{0} received state information after hang, remove "
                        "from the list.".format(self.device_name))
                    self.remove_from_hang(self.device_name)
                    self._hang_recovery_ctr = 0
            except Exception as e:
                self.error_stream(
                    "{0}: Exception processing a newer state (restoring the "
                    "previous {1}): {2}".format(
                        self.device_name, old_state, e))
                self._device_state = old_state
            self.info_stream(
                "{0} store newer state {1}".format(
                    self.device_name, self.device_state))
        # else: nothing change, nothing to do.

    def __state_has_changed(self, new_state):
        return new_state != self.device_state

    def __was_running(self):
        return self.device_state == DevState.RUNNING

    def __was_in_fault(self):
        return self.device_state == DevState.FAULT

    # --- threading

    def __hang_monitor_thread(self, start_delay):
        if start_delay > 0:
            self.info_stream(
                "{0} watchdog build, wait {1:g} until start".format(
                    self.device_name, start_delay))
            sleep(start_delay)
        self.info_stream(
            "{0} launch background monitor".format(self.device_name))
        while not self._joiner_event.isSet():
            t_0 = time()
            self.__manual_check()
            self.__wait_next_check(time()-t_0)

    def __manual_check(self):
        for i in range(2):
            state = self.__state_request()
            if state:
                self.debug_stream(
                    "{0} respond state {1}".format(self.device_name, state))
                break
        else:
            state = None
        if not state:  # no answer from the device
            if not self.is_in_hang_state_list(self.device_name):
                self.debug_stream(
                    "{0} no state information.".format(self.device_name))
                self.__unsubscribe_event()
                self.append_to_hang(self.device_name)
            # review any other list where it can be
            if self.is_in_running_state_list(self.device_name):
                self.remove_from_running(self.device_name)
            if self.is_in_fault_state_list(self.device_name):
                self.remove_from_fault(self.device_name)
                self._fault_recovery_ctr = 0
            if self.device_state is None and self.try_hang_recovery:
                self.debug_stream(
                    "{0} not state information by a second try.".format(
                        self.device_name))
                # force to launch the recover after a second loop
                self.__hang_recovery_procedure()
            self._device_state = None
        else:
            if self.device_state is None:
                self.debug_stream(
                    "{0} gives state information, back from hang.".format(
                        self.device_name))
                if self.is_in_hang_state_list(self.device_name):
                    self.remove_from_hang(self.device_name)
                    self._hang_recovery_ctr = 0
                self._device_state = state
                self.__build_proxy()
            if state == DevState.FAULT and self.try_fault_recovery:
                self.__fault_recovery_procedure()
            if self.device_state != state:
                # state has change but hasn't been cached by events
                self._device_state = state

    def __state_request(self):
        try:
            return self.device_proxy.State()
        except Exception as exception:
            # Canary to know and specialize
            self.warn_stream(
                "{0} don't respond state request ({1})".format(
                    self.device_name, type(exception)))
            return None

    def __wait_next_check(self, delta_t):
        if delta_t < self._recheck_period:
            to_sleep = self._recheck_period-delta_t
            self.debug_stream(
                "{0} monitor's thread required {1:g} seconds "
                "(go sleep for {2:g} seconds)".format(
                    self.device_name, delta_t, to_sleep))
            self._overlaps = 0
            sleep(to_sleep)
        else:
            self._overlaps += 1
            if self._overlaps % self._overlaps_alert:
                self.warn_stream(
                    "{0} hang check has take more than loop time "
                    "({1:g} seconds). No sleep for another check.".format(
                        self.device_name, delta_t))
            else:  # when modulo self._overlapsAlert == 0
                self.warn_stream(
                    "{0} hang check has take more than loop time "
                    "({1:g} seconds). But {2:d} consecutive, forcing "
                    "to sleep some time.".format(
                        self.device_name, delta_t, self._overlaps))
                self.mailto(
                    "Recheck overlaps",
                    "There has been {0} consecutive overlaps in the recheck "
                    "thread".format(self._overlaps))
                sleep(self._recheck_period)

    def __fault_recovery_procedure(self):
        status_message = None
        try:
            if self._device_proxy:
                status_message = self._device_proxy.status()
                state = self.__state_request()
                if state == DevState.FAULT:
                    self._device_proxy.Init()
                else:
                    self.error_stream(
                        "Fault recovery procedure requested to {0} when it "
                        "is not in FAULT state".format(self.device_name))
                    self.remove_from_fault(self.device_name)
                    self._fault_recovery_ctr = 0
                    self._report_fault_procedure(
                        "No init recommended, found in state {0}".format(state),
                        status_message)
                    return
            else:
                self.warn_stream(
                    "{0} no proxy to command Init()".format(self.device_name))
        except Exception as exception_obj:
            self.error_stream(
                "{0} in Fault recovery procedure Exception: {1}".format(
                    self.device_name, exception_obj))
            self._report_fault_procedure(exception_obj, status_message)
        else:
            self.debug_stream("{0} Init() completed".format(self.device_name))
            self._report_fault_procedure(None, status_message)
        self._fault_recovery_ctr += 1

    def __hang_recovery_procedure(self):
        # if Astor is None:
        #     self.error_stream("This feature is only available with "
        #                       "fandango's Astor present")
        #     return
        instance = None
        try:
            astor = Astor()
            instance = astor.get_device_server(self.device_name)
            self.info_stream("Recovery for {0} with instance {1}"
                             "".format(self.device_name, instance))
            if not instance:
                raise Exception(
                    "Astor didn't solve the device server instance ({0})"
                    "".format(instance))
            if not self.__force_restart_instance(astor, instance):
                self.error_stream(
                    "{0} Astor cannot recover".format(self.device_name))
        except Exception as exception_obj:
            self.error_stream(
                "{0} __hang_recovery_procedure() Exception: {1}".format(
                    self.device_name, exception_obj))
            if instance is not None:
                self._report_hang_procedure(instance, exception_obj)
        else:
            if instance is not None:
                self._report_hang_procedure(instance, None)
        self._hang_recovery_ctr += 1

    def __force_restart_instance(self, astor, instance):
        for i in range(DEFAULT_ASTOR_nSTOPS):
            self.info_stream("stop instance {0} ({1})".format(instance, i))
            try:
                res = astor.stop_servers([instance])
            except AssertionError as exception_obj:
                self.error_stream("{0}".format(exception_obj))
                if hasattr(astor, "not_found_servers_on_check"):
                    # This depends on if it has used fandango or
                    # the minimal implementation
                    self.warn_stream("{0} not on {1}".format(
                        astor.not_found_servers_on_check,
                        astor.all_servers_on_check))
                res = False
            if res:
                self.info_stream("stop instance {0} done".format(instance))
                break
            sleep(DEFAULT_ASTOR_STOPWAIT)
        self.debug_stream(
            "{0} Astor start {1}".format(self.device_name, instance))
        return astor.start_servers([instance])

    def _report_fault_procedure(self, exception_obj, status_message):
        if self._fault_recovery_ctr == 0:
            # only report when it has happen, no remainders
            mail_body = ["Applied the recovery from Fault procedure.\n",
                         "\nAffected camera was: {0}".format(self.device_name)]
            if exception_obj:
                mail_body.append(
                    "\nEncountered exceptions during the process:\n{0}"
                    "".format(exception_obj))
            if status_message:
                mail_body.append(
                    "\n\nStatus before the Init(): {0!r}".format(
                        status_message))
                self._device_status = status_message
            if len(self._extra_attribute_values) > 0:
                mail_body.append("\nextra attributes:")
                for attr_name, attr_value in \
                        self._extra_attribute_values.items():
                    mail_body.append("\n\t{0} = {1}".format(
                        attr_name, attr_value))
            mail_body.append("\n--\nEnd transmission.")
            self.mailto("Device in FAULT state", ''.join(mail_body))

    def _report_hang_procedure(self, instance, exception_obj):
        if self._hang_recovery_ctr == 0:
            # only report when it has happen, no remainders
            mail_body = ["Applied the recovery from Hang procedure.\n",
                         "\nAffected camera was: {0}".format(self.device_name)]
            if instance:
                mail_body.append(" (instance: {0})".format(instance))
            if len(self._extra_attribute_values) > 0:
                mail_body.append("\nextra attributes:")
                for attr_name, attr_value in \
                        self._extra_attribute_values.items():
                    mail_body.append("\n\t{0} = {1}".format(
                        attr_name, attr_value))
            if exception_obj:
                mail_body.append(
                    "\nEncountered exceptions during the process:\n{0}".format(
                        exception_obj))
            mail_body.append("\n--\nEnd transmission.")
            self.mailto("Device HANG", ''.join(mail_body))


class WatchdogTester:
    def __init__(self, device_list, joiner_event, mailto, *args, **kwargs):
        if version_info.major >= 3:
            super(WatchdogTester, self).__init__(*args, **kwargs)
        else:
            pass
        self._monitors_list = []
        self._running_list = []
        self._fault_list = []
        self._hang_list = []
        for device_name in device_list:
            dog = Dog(device_name, joiner_event, self)
            dog.try_fault_recovery = True
            dog.try_hang_recovery = True
            self._monitors_list.append(dog)
        self.__mailto = mailto

    @staticmethod
    def error_stream(msg):
        print("ERROR:\t{0}".format(msg))

    @staticmethod
    def warn_stream(msg):
        print("WARN:\t{0}".format(msg))

    @staticmethod
    def info_stream(msg):
        print("INFO:\t{0}".format(msg))

    @staticmethod
    def debug_stream(msg):
        print("DEBUG:\t{0}".format(msg))

    def is_in_running_list(self, who):
        self.is_in_list(self._running_list, who)

    def append_to_running(self, who):
        self.append_to_list(self._running_list, "running", who)

    def remove_from_running(self, who):
        self.remove_from_list(self._running_list, "running", who)

    def is_in_fault_list(self, who):
        self.is_in_list(self._fault_list, who)

    def append_to_fault(self, who):
        self.append_to_list(self._fault_list, "fault", who)

    def remove_from_fault(self, who):
        self.remove_from_list(self._fault_list, "fault", who)

    def is_in_hang_list(self, who):
        self.is_in_list(self._hang_list, who)

    def append_to_hang(self, who):
        self.append_to_list(self._hang_list, "hang", who)

    def remove_from_hang(self, who):
        self.remove_from_list(self._hang_list, "hang", who)

    @staticmethod
    def is_in_list(lst, who):
        return lst.count(who)

    def append_to_list(self, list_obj, list_name, who):
        if not list_obj.count(who):
            list_obj.append(who)
            self.debug_stream(
                "{0} append to {1} list".format(who, list_name))
        else:
            self.warn_stream(
                "{0} was already in the {1} list".format(who, list_name))

    def remove_from_list(self, list_obj, list_name, who):
        if list_obj.count(who):
            list_obj.pop(list_obj.index(who))
            self.debug_stream(
                "{0} removed from {1} list".format(who, list_name))
        else:
            self.warn_stream(
                "{0} was NOT in the {1} list".format(who, list_name))

    @property
    def MailTo(self):
        return self.__mailto

    @staticmethod
    def get_name():
        return "WatchdogTester"

    def mailto(self, action, msg):
        if len(self.MailTo) != 0:
            name = self.get_name()
            mail = email.mime.text.MIMEText(msg)
            mail['From'] = "{0}@{1}".format(name, gethostname())
            mail['To'] = ', '.join(self.MailTo)
            mail['Subject'] = "[{0}] {1}".format(self.get_name(), action)
            s = smtplib.SMTP('localhost')
            s.sendmail(mail['From'], self.MailTo, mail.as_string())
            s.quit()
            self.debug_stream("Email sent...")


def main():
    from optparse import OptionParser
    import signal
    import sys

    def signal_handler(signal, frame):
        print('\nYou pressed Ctrl+C!\n')
        sys.exit(0)

    parser = OptionParser()
    parser.add_option('', "--devices",
                      help="List of device names to provide to the tester")
    parser.add_option('', "--mailto", dest="mailto", default="",
                      help="Email address to test to mail send")
    (options, args) = parser.parse_args()
    if options.devices:
        signal.signal(signal.SIGINT, signal_handler)
        print("\n\tPress Ctrl+C to finish\n")
        joiner_event = Event()
        joiner_event.clear()
        tester = WatchdogTester(options.devices.split(','),
                                joiner_event,
                                options.mailto)
        signal.pause()
        del tester


if __name__ == '__main__':
    main()
