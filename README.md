# WatchDogDS

Tango device to monitor and automatic recover tango devices (initially thought for the ImgGrabber)

## Start testing on debian 9 with python3

To install this project from sources:

```bash
python3 setup.py develop --user
```

The uninstall can be made:

```bash
python3 setup.py develop --uninstall --user
```

This device is integrated with the Alba's CI, so debian packages shall be 
build for each tag. 

Check the instances in you network:

```bash
Watchdog -?
```

Launch your instance with some runtime information:

```bash
Watchdog <instance_name> -v2
```


## Tango Device Server

This device server has a set of properties to be configured. It doesn't 
requires to work only with ImgGrabber device class, but it is the starting use.

The properties are:
* DevicesList: String list with comma separated and line separated set of 
  devices to be watched
* ExtraAttrList: List of attributes to be watched from the devices. Each 
  will have its relay attribute for each of the devices, together with 
  single attribute listing the values of all the watched devices
* MailTo: to address where emails will be sent when reporting
* ReportPeriod: apart from "on event" reports (fault, hand or recoveries) it 
  will send a summary of what happen during the hours said in this property.
* Try{Fault,Hand}Recover: boolean property to block or allow the watchdog to 
  act when these situations happen to a monitored device.
* DealerAttrList: Secondary functionality to set up values to devices based 
  in set information.

### Functionalities

#### {Fault,Hang} recovery

The main functionality is to act over a set of tango devices when some 
situations happen to them. The origin of this device is to reduce the work 
load created by an unstable implementation with device that hangs (or decay 
to fault) from time to time and, being many, this happens to one or another 
too often.

#### Dealer

A secondary functionality has been implemented because when we have very 
many cameras triggered at the exactly same time, they send to the channel 
(network) all the images at the same time, so they produce a severe 
congestion in a much smaller time base than usual network. 

That is, even the flow rate should absorb it, it's saying a time laps of a 
second when this happens on few milliseconds.

